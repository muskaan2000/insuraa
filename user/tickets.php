<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['uid'] == 0)) {
    header('location:logout.php');
} else {

    if (isset($_POST['submit'])) {
        $uid = $_SESSION['uid'];
        $sub = $_POST['subject'];
        $noi = $_POST['issue'];

        $des = $_POST['description'];


        $query = mysqli_query($con, "insert into tblticket(UserId, Subject, NatureofIssue, Description) value('$uid', '$sub','$noi','$des')");
        if ($query) {
            $msg = "Ticket has been raised.";
        } else {
            $msg = "Something Went Wrong. Please try again";
        }


    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Ticket Form!!</title>

    </head>
    <body class="dashboard">
    <?php include_once('includes/sidebar.php'); ?>

    <?php include_once('includes/header.php'); ?>
    <section class="ticket">
        <div class="container">
            <div class="content-header">
                <div>
                    <h2 class="content-title">Ticket Form !</h2>
                </div>
            </div>
            <div class="content-body">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="card">
                            <form method="post">
                                <p style="font-size:16px; color:red" align="left"> <?php if ($msg) {
                                        echo $msg;
                                    } ?> </p>
                                <div class="d-flex flex-column wd-md-500 pd-30 pd-sm-40 bg-gray-200">

                                    <div class="form-group">
                                        <label class="form-label">Subject: <span class="tx-danger">*</span></label>
                                        <input type="text" name="subject" class="form-control wd-450" required="true">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label">Nature of Issue: <span
                                                    class="tx-danger">*</span></label>
                                        <select name="issue" class="form-control wd-450" required="true">
                                            <option value="">Nature Of Issue</option>
                                            <option value="payment issue" selected="true">Payment Of Issue</option>
                                            <option value="policy of issue">Policy Of Issue</option>
                                            <option value="claim  issue">Claim Issue</option>
                                            <option value="profile wrong updation">Profile Wrong Updation</option>
                                            <option value="Other">Other</option>

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label">Description: <span class="tx-danger">*</span></label>
                                        <textarea name="description" placeholder="Desribe your Issue" rows="12"
                                                  cols="14"
                                                  class="form-control wd-450" required="true"></textarea>
                                    </div>


                                    <div class="form-group" align="center">
                                        <button type="submit" name="submit" class="btn-default">Submit
                                        </button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                </form>

            </div>
        </div>

    </section>

    <?php include_once('includes/footer.php'); ?>


    </body>
    </html>
<?php } ?>
