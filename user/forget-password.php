<?php include_once('../header.php'); ?>

<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);

if(isset($_POST['submit']))
  {
    $contactno=$_POST['contactno'];
    $email=$_POST['email'];

        $query=mysqli_query($con,"select ID from tbluser where  Email='$email' and ContactNo='$contactno' ");
    $ret=mysqli_fetch_array($query);
    if($ret>0){
      $_SESSION['contactno']=$contactno;
      $_SESSION['email']=$email;
     header('location:resetpassword.php');
    }
    else{
      $msg="Invalid Details. Please try again.";
    }
  }
  ?>




<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Insuraa | Forget Password</title>
      

  </head>
  <body>
  <header class="main-nav shadow">
      <div class="container">
          <header class="main-nav shadow">
              <div class="container">
                  <nav class="navbar navbar-expand-sm navbar-dark inner-nav">
                      <a class="navbar-brand" href="/">INSURAA</a>
                      <ul class="navbar-nav">
                          <li class="nav-item">
                              <a class="nav-link" href="/">Home</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/about.php">About</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/calculator.php">Calculator</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/contact.php">Contact</a>
                          </li>
                          <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="/user/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Login
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="/user/index.php">User Login</a>
                                  <a class="dropdown-item" href="/user/signup.php">User Sign Up</a>
                                  <a class="dropdown-item" href="/admin/index.php">Admin</a>
                              </div>
                          </li>
                      </ul>
                  </nav>
              </div>
          </header>

      </div>
  </header>

    <section class="signin">
      <div class="form-wrap">
        <div class="wrapper">
          <h2>Recover Your Password!</h2>
         
          <p style="font-size:16px; color:red" align="center"> <?php if($msg){
    echo $msg;
  }  ?> </p>

          <form name="login" method="post">
            <div class="form-group">
              <label>Email</label>
      <input type="email" class="form-control" placeholder="Enter your email"  name="email" required="true">
            </div>
            <div class="form-group">
              <label>Contact Number</label>
    <input type="text" class="form-control" placeholder="Enter your Contact Number" name="contactno" required="true">
            </div>
            <button class="btn-default" type="submit" name="submit">Reset</button>
          </form>
            <div class="signin-footer">
                <p>Don't have an account? <a href="signup.php">Create an Account</a></p>
            </div>
        </div>

      </div>
    </section>
  <?php include_once('../footer.php'); ?>

    <script>
      $(function(){
        'use strict'

      });
    </script>
  </body>
</html>
