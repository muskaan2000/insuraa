<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);
if (strlen($_SESSION['uid']==0)) {
  header('location:logout.php');
  } else{


if(isset($_POST['submit']))
  {
   
    $pid=$_SESSION['uid'];
    $FName=$_POST['fullname'];
    $ContactNo=$_POST['contactnumber'];
    $email=$_POST['email'];
    $gender=$_POST['gender'];
     $query=mysqli_query($con, "update tbluser set FullName='$FName',  ContactNo='$ContactNo',  Email='$email', Gender='$gender' where ID='$pid'");
    if ($query) {
    $msg="Your profile has been updated.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again.";
    }
  }
  ?>


<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Insuraa |  Update User Detail</title>



  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="profile">
    <div class="container">
        <div class="content-header">

            <h2 class="content-title ">Update User Profile !</h2>

        </div>
        <div class="content-body">
                    <div class="card">
                        <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                                echo $msg;
                            }  ?> </p>
                        <form method="post" action="">
                            <?php
                            $pid=$_SESSION['uid'];
                            $ret=mysqli_query($con,"select * from tbluser where ID='$pid'");
                            $cnt=1;
                            while ($row=mysqli_fetch_array($ret)) {

                            ?>
                            <div class="wsm-600">
                                <div class="mflex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Full Name <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter your Full Name"  name="fullname" required="true" value="<?php  echo $row['FullName'];?>">
                                    </div></div>

                                <div class="mflex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Contact Number <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter your Contact Number"  name="contactnumber" maxlength="10" pattern="[0-9]+" required="true" value="<?php  echo $row['ContactNo'];?>">
                                    </div></div>
                                <div class="mflex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Email <span class="tx-danger">*</span></label>
                                        <input type="email" class="form-control" placeholder="Enter your Email"  name="email"  required="true" value="<?php  echo $row['Email'];?>">
                                    </div></div>

                                <div class="mflex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Gender <span class="tx-danger">*</span></label>
                                        <?php if($row['Gender']=="Male")
                                        {?>
                                            <input type="radio" id="gender" name="gender" value="Male" checked="true">Male

                                            <input type="radio" name="gender" value="Female">Female
                                        <?php }   if($row['Gender']=="Female") {?>
                                            <input type="radio" id="gender" name="gender" value="Male" >Male
                                            <input type="radio" name="gender" value="Female" checked="true">Female
                                        <?php }?>
                                    </div></div>

                                <?php } ?>



                                <button type="submit" name="submit" class="btn-default">Update</button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
</section>



    <?php include_once('includes/footer.php');?>


    
  </body>
</html>
<?php }  ?>