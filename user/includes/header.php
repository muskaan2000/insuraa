<div class="dash-header">
    <div class="container">
        <div class="header-left">
            <a href="" id="SidebarToggle" class="header-menu-icon"><span></span></a>
        </div>

        <div class="header-right">
            <div class="dropdown profile-menu">
                <a href="" class="img-user"><img src="/img/user.svg" alt=""></a>
                <div class="dropdown-menu">
                    <div class="dropdown-header">
                        <a href="" class="header-arrow"><i class="icon ion-md-arrow-back"></i></a>
                    </div>
                    <div class="header-profile">
                        <div class="img-user">
                            <img src="/img/user.svg" alt="user">
                            <?php
                            $uid = $_SESSION['uid'];
                            $ret = mysqli_query($con, "select FullName from tbluser where ID='$uid'");
                            $row = mysqli_fetch_array($ret);
                            $name = $row['FullName'];

                            ?>
                            <span style="font-weight: bold"><?php echo $name; ?></span>
                        </div>
                        <a href="/user/user-profile.php" class="dropdown-item"><i class="typcn typcn-user-outline"></i>
                            My Profile</a>
                        <a href="/user/changepassword.php" class="dropdown-item"><i class="typcn typcn-cog-outline"></i>
                            Change Password</a>
                        <a href="/user/logout.php" class="dropdown-item"><i class="typcn typcn-power-outline"></i> Sign
                            Out</a>
                    </div>
                </div>
            </div>
        </div>
    </div>