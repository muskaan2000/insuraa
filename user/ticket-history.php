<?php include_once('../header.php'); ?>

<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['uid']==0)) {
  header('location:logout.php');
  } else{

?>





<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Insuraa |  Ticket History</title>



  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="user">
    <div class="container">
        <div class="content-header ">
            <h2 class="content-title ">Ticket History!</h2>
        </div>
        <div class="content-body">

            <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                    echo $msg;
                }  ?> </p>
            <div class="table-responsive">
                <table class="table table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Subject</th>
                        <th>Nature of Issue</th>
                        <th>Ticket Generation Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <?php
                    $tid=$_SESSION['uid'];
                    $ret=mysqli_query($con,"select * from tblticket where UserId=$tid");
                    $cnt=1;
                    while ($row=mysqli_fetch_array($ret)) {

                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo $cnt;?></td>
                        <td><?php  echo $row['Subject'];?></td>
                        <td><?php  echo $row['NatureofIssue'];?></td>
                        <td><?php  echo $row['TicketGenerationDate'];?></td>

                        <td><a href="ticket-view.php?ticid=<?php echo $row['ID'];?>">View Ticket Detail</a>
                    </tr>
                    <?php
                    $cnt=$cnt+1;
                    }?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>


    <?php include_once('includes/footer.php');?>




  </body>
</html>
<?php }  ?>