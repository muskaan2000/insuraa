<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
include('../header.php');
if(isset($_POST['submit']))
  {
    $fname=$_POST['fullname'];
    $contno=$_POST['contactnumber'];
    $email=$_POST['email'];
    $gen=$_POST['gender'];
    $Password=$_POST['password'];

    $ret=mysqli_query($con, "select Email from tbluser where Email='$email'");
    $result=mysqli_fetch_array($ret);
    if($result>0){
$msg="This email already associated with another account";
    }
    else{
    $query=mysqli_query($con, "insert into tbluser(FullName, ContactNo, Email, Gender,  Password) value('$fname', '$contno', '$email','$gen', '$Password' )");
    if ($query) {
    $msg="You have successfully registered";
  }
  else
    {
      $msg="Something Went Wrong. Please try again";
    }
}
}

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Insuraa | User Signup</title>
<script type="text/javascript">
function checkpass()
{
if(document.signup.password.value!=document.signup.repeatpassword.value)
{
alert('Password and Repeat Password field does not match');
document.signup.repeatpassword();
return false;
}
return true;
} 

</script>
  </head>
  <body>

  <header class="main-nav shadow">
      <div class="container">
          <header class="main-nav shadow">
              <div class="container">
                  <nav class="navbar navbar-expand-sm navbar-dark inner-nav">
                      <a class="navbar-brand" href="/">INSTIVE</a>
                      <ul class="navbar-nav">
                          <li class="nav-item">
                              <a class="nav-link" href="/">Home</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/about.php">About</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/calculator.php">Calculator</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/contact.php">Contact</a>
                          </li>
                          <li class="nav-item dropdown">
                              <a class="nav-link active dropdown-toggle" href="/user/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Login
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="/user/index.php">User Login</a>
                                  <a class="dropdown-item" href="/user/signup.php">User Sign Up</a>
                                  <a class="dropdown-item" href="/admin/index.php">Admin</a>
                              </div>
                          </li>
                      </ul>
                  </nav>
              </div>
          </header>
      </div>
  </header>


<section class="sign-up">
    <div class="form-wrap">
        <div class="wrapper">
            <div class="signup-header">
                <h2>Get Started</h2>
                <h4>It's free to signup and only takes a minute.</h4>

                <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                        echo $msg;
                    }  ?> </p>

                <form  method="post" name="signup" onsubmit="return checkpass();">
                    <div class="form-group">
                        <label>Fullname</label>
                        <input type="text" class="form-control" placeholder="Enter your Full Name"  name="fullname" required="true">
                    </div>
                    <div class="form-group">
                        <label>Contact Number</label>
                        <input type="text" class="form-control" placeholder="Enter your Contact Number"  name="contactnumber" maxlength="10" pattern="[0-9]+" required="true">
                    </div>

                    <div class="form-group">
                        <label>Email id</label>
                        <input type="email" class="form-control" placeholder="Enter your Email"  name="email"  required="true">
                    </div>

                    <div class="form-group">
                        <label>Gender</label>
                        <input type="radio" name="gender" value="Female" checked="true">Female &nbsp;<input type="radio" name="gender" value="male" checked="true">Male
                    </div>


                    <div class="form-group">
                        <label>Passowrd</label>
                        <input type="password" class="form-control" id="password" placeholder="Enter your password" name="password" required="true">
                    </div>


                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" placeholder="Repeat Password" name="repeatpassword" id="repeatpassword" required="true">
                    </div>


                    <button class="btn btn-primary btn-block" type="submit" name="submit">Create Account</button>

                </form>
            </div><!-- signup-header -->
            <div class="signup-footer">
                <p>Already have an account? <a href="index.php">Sign In</a></p>
            </div><!-- signin-footer -->
        </div><!-- column-signup -->
    </div>
</section>

    <script>
      $(function(){
        'use strict'

      });
    </script>

    <?php
    include('../footer.php');
    ?>
  </body>
</html>
