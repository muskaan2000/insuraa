<?php include_once('../header.php'); ?>

<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);

if(isset($_POST['submit']))
  {
    $contactno=$_SESSION['contactno'];
    $email=$_SESSION['email'];
    $password=$_POST['newpassword'];

        $query=mysqli_query($con,"update tbluser set Password='$password'  where  Email='$email' && ContactNo='$contactno' ");
   if($query)
   {
echo "<script>alert('Password successfully changed');</script>";
session_destroy();
   }
  
  }
  ?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Insuraa | Reset Password</title>
<script type="text/javascript">
function checkpass()
{
if(document.changepassword.newpassword.value!=document.changepassword.confirmpassword.value)
{
alert('New Password and Confirm Password field does not match');
document.changepassword.confirmpassword();
return false;
}
return true;
} 

</script>
  </head>
  <body class="body">

  <header class="main-nav shadow">
      <div class="container">
          <header class="main-nav shadow">
              <div class="container">
                  <nav class="navbar navbar-expand-sm navbar-dark inner-nav">
                      <a class="navbar-brand" href="/">INSTIVE</a>
                      <ul class="navbar-nav">
                          <li class="nav-item">
                              <a class="nav-link active" href="/">Home</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/about.php">About</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/calculator.php">Calculator</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/contact.php">Contact</a>
                          </li>
                          <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="/user/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Login
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="/user/index.php">User Login</a>
                                  <a class="dropdown-item" href="/user/signup.php">User Sign Up</a>
                                  <a class="dropdown-item" href="/admin/index.php">Admin</a>
                              </div>
                          </li>
                      </ul>
                  </nav>
              </div>
          </header>
      </div>
  </header>


    <section  class="signin">
      <div class="form-wrap">
        <div class="wrapper">
          <h2>Reset Your Password!</h2>
          
          <p style="font-size:16px; color:red" align="center"> <?php if($msg){
    echo $msg;
  }  ?> </p>

          <form name="changepassword" method="post" onsubmit="return checkpass();">
            <div class="form-group">
              <label>New Password</label>
      <input type="password" class="form-control" placeholder="Enter your New Password"  name="newpassword" required="true">
            </div>
            <div class="form-group">
              <label>Confirm Password</label>
    <input type="password" class="form-control" placeholder="Confirm Your Password" name="confirmpassword" required="true">
            </div>
            <button class="btn-default" type="submit" name="submit">Reset</button>
          </form>
            <div class="signin-footer">

                <p>Don't have an account? <a href="signup.php">Create an Account</a></p>
            </div>
        </div>


      </div>
    </section>

    <script>
      $(function(){
        'use strict'

      });
    </script>
  <?php include_once('../footer.php'); ?>

  </body>
</html>
