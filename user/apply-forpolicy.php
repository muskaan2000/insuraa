<?php include_once('../header.php'); ?>

<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['uid'] == 0)) {
    header('location:logout.php');
} else {

    if (isset($_GET['polid'])) {
        $userid = $_SESSION['uid'];
        $pid = $_GET['polid'];
        $status = 0;
        $policynumber = mt_rand(100000000, 999999999);
        $ret = mysqli_query($con, "select ID from tblpolicyholder where UserId='$userid' and PolicyId='$pid'");
        $row = mysqli_fetch_array($ret);
        if ($row > 0) {
            $msg = "You already applied for this Policy. ";
        } else {


            $query = mysqli_query($con, "insert into  tblpolicyholder(UserId,PolicyId,PolicyStatus,PolicyNumber) value('$userid','$pid','$status','$policynumber')");
            $msg = "You have successfully applied for Policy. ";

        }
    }
    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>


        <title>Insuraa | Apply for Policy </title>


    </head>
    <body class="dashboard">
    <?php include_once('includes/sidebar.php'); ?>
    <?php include_once('includes/header.php'); ?>

<section class="policy">
    <div class="content-header">
        <h2 class="content-title">Apply for Policy !</h2>
    </div>
    <div class="content-body">
        <div class="container">
            <div class="content-body">

                <div class="content-label mg-b-5">Policy Detail Details</div>
                <p style="font-size:16px; color:red" align="center"> <?php if ($msg) {
                        echo $msg;
                    } ?> </p>
                <div class="table-responsive">
                    <table class="table table-bordered mg-b-0">
                        <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Policy Name</th>

                            <th>Category Name</th>
                            <th>SubCategory Name</th>

                            <th>Sum Assured</th>
                            <th>Premium</th>
                            <th>Tenure</th>
                            <th>Creation Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <?php
                        $ret = mysqli_query($con, "select category.CategoryName as catname,tblsubcategory.SubcategoryName as subcat, tblpolicy.PolicyName,tblpolicy.CreationDate as cdate,tblpolicy.ID,tblpolicy.Sumassured,tblpolicy.Premium,tblpolicy.Tenure  from tblpolicy inner join category on category.ID=tblpolicy.CategoryId inner join tblsubcategory on  tblsubcategory.id=tblpolicy.SubcategoryId");
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($ret)) {

                        ?>
                        <tbody>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><?php echo $row['PolicyName']; ?></td>

                            <td><?php echo $row['catname']; ?></td>
                            <td><?php echo $row['subcat']; ?></td>

                            <td><?php echo $row['Sumassured']; ?></td>
                            <td><?php echo $row['Premium']; ?></td>
                            <td><?php echo $row['Tenure']; ?></td>
                            <td><?php echo $row['cdate']; ?></td>
                            <td><a href="apply-forpolicy.php?polid=<?php echo $row['ID']; ?>">Apply </a></td>
                        </tr>
                        <?php
                        $cnt = $cnt + 1;
                        } ?>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</section>

        <?php include_once('includes/footer.php'); ?>

    </body>
    </html>
<?php } ?>