<?php include_once('../header.php'); ?>

<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['uid']==0)) {
  header('location:logout.php');
  } else{

  ?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Ticket Form!!</title>

  </head>



  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="ticket">
<div class="container">
    <div class="content-header">
        <div>
            <h2 class="content-title ">Ticket Form !</h2>
        </div>
    </div>
    <div class="content-body">


        <div class="row ">
            <div class="col-md-12 ">
                <div class="card card-table-two">
                    <form method="post">
                        <p style="font-size:16px; color:red" align="left"> <?php if($msg){
                                echo $msg;
                            }  ?> </p>
                        <?php
                        $cid=$_GET['ticid'];
                        $ret=mysqli_query($con,"select * from tblticket where ID='$cid'");
                        $cnt=1;
                        while ($row=mysqli_fetch_array($ret)) {

                            ?>

                            <table border="1" class="table table-bordered mg-b-0">
                                <tr>
                                    <th>Ticket ID :</th>
                                    <td><?php  echo $row['ID'];?></td>
                                </tr>
                                <tr>
                                    <th>Ticket Generation Date</th>
                                    <td><?php  echo $row['TicketGenerationDate'];?></td>
                                </tr>
                                <tr>
                                    <th>Subject</th>
                                    <td><?php  echo $row['Subject'];?></td>
                                </tr>

                                <tr>
                                    <th>Nature Of Issue</th>
                                    <td><?php  echo $row['NatureofIssue'];?></td>
                                </tr>

                                <tr>
                                    <th>Description</th>
                                    <td><?php  echo $row['Description'];?></td>
                                </tr>

                                <tr>
                                    <th>Admin Remark</th>
                                    <td><?php
                                        if($row['AdminRemark']==""){
                                            echo "No action taken yet";
                                        } else {
                                            echo $row['AdminRemark'];
                                        }?></td>
                                </tr>




                                <tr>
                                    <th>Admin Remark date</th>
                                    <td>
                                        <?php
                                        if($row['AdminRemarkdate']==""){
                                            echo "No action taken yet";
                                        } else {
                                            echo $row['AdminRemarkdate'];
                                        }?>

                                    </td>
                                </tr>



                            </table>

                        <?php } ?>
</div>

</body>
</html>

<?php } ?>