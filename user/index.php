<?php include('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');

if(isset($_POST['signin']))
  {
    $email=$_POST['email'];
    $password=$_POST['password'];
    $query=mysqli_query($con,"select ID from tbluser where  Email='$email' && Password='$password' ");
    $ret=mysqli_fetch_array($query);
    if($ret>0){
      $_SESSION['uid']=$ret['ID'];
     header('location:dashboard.php');
    }
    else{
    $msg="Invalid Details.";
    }
  }
  ?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Insurance Management System in PHP and MySQL">
    <meta name="author" content="Muskaan Madaan">
    <title>Insuraa | User Login</title>
  </head>

  <header class="main-nav shadow">
      <div class="container">
          <header class="main-nav shadow">
              <div class="container">
                  <nav class="navbar navbar-expand-sm navbar-dark inner-nav">
                      <a class="navbar-brand" href="/">INSURAA</a>
                      <ul class="navbar-nav">
                          <li class="nav-item">
                              <a class="nav-link" href="/">Home</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/about.php">About</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/calculator.php">Calculator</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="/contact.php">Contact</a>
                          </li>
                          <li class="nav-item dropdown">
                              <a class="nav-link active dropdown-toggle" href="/user/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Login
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="/user/index.php">User Login</a>
                                  <a class="dropdown-item" href="/user/signup.php">User Sign Up</a>
                                  <a class="dropdown-item" href="/admin/index.php">Admin</a>
                              </div>
                          </li>
                      </ul>
                  </nav>
              </div>
          </header>

      </div>
  </header>


  <body class="body">

  <section class="signin form-wrap">
      <div class="container">
          <div class="wrapper">
              <div class="signin-head">
                  <h2>Welcome back!</h2>
                  <h4>Please sign in to continue</h4>
                  <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                          echo $msg;
                      }  ?> </p>

                  <form name="login" method="post">
                      <div class="form-wrap">
                          <label>Email</label>
                          <input type="email" class="form-control" placeholder="Enter your email"  name="email" required="true">
                      </div>
                      <div class="form-group">
                          <label>Password</label>
                          <input type="password" class="form-control" placeholder="Enter your password" name="password" required="true">
                      </div>
                      <button class="btn-default " type="submit" name="signin">Sign In</button>
                  </form>
              </div>

              <div class="signin-footer">
                  <p><a href="forget-password.php">Forgot password?</a></p>
                  <p>Don't have an account? <a href="signup.php">Create an Account</a></p>
              </div>
          </div>
      </div>
  </section>


    <script>
      $(function(){
        'use strict'

      });
    </script>
  <?php
  include '../footer.php';
  ?>

  </body>
</html>
