<?php include ('header.php'); ?>
<header class="main-nav shadow">
    <div class="container">
        <header class="main-nav shadow">
            <div class="container">
                <nav class="navbar navbar-expand-sm navbar-dark inner-nav">
                    <a class="navbar-brand" href="/">INSURAA</a>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about.php">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/calculator.php">Calculator</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact.php">Contact</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="/user/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Login
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/user/index.php">User Login</a>
                                <a class="dropdown-item" href="/user/signup.php">User Sign Up</a>
                                <a class="dropdown-item" href="/admin/index.php">Admin</a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

    </div>
</header>


    <section class="hero-banner">
        <div class="bg-img">
            <img src="/img/services.jpg" alt="Buildings">
        </div>
        <span class="bg-overlay"></span>
        <div class="text-wrap">
            <h2>Services</h2>
        </div>
    </section>

<section class="emi-calculator">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <img src="/img/calci.png" alt="calci">
            </div>
            <div class="col-6">
                <div class="form-wrap">
                    <form>
                        <label>Total Amt:</label>
                        <input type="text">
                        <label>Rate of Interest</label>
                        <input type="text">
                        <label>Number of Months</label>
                        <input type="text">
                        <input type="submit" class="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ('footer.php'); ?>