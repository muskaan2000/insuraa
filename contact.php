<?php include('header.php'); ?>

<header class="main-nav shadow">
    <div class="container">
        <header class="main-nav shadow">
            <div class="container">
                <nav class="navbar navbar-expand-sm navbar-dark inner-nav">
                    <a class="navbar-brand" href="/">INSURAA</a>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about.php">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/calculator.php">Calculator</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/contact.php">Contact</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="/user/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Login
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/user/index.php">User Login</a>
                                <a class="dropdown-item" href="/user/signup.php">User Sign Up</a>
                                <a class="dropdown-item" href="/admin/index.php">Admin</a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

    </div>
</header>
<section class="hero-banner">
    <div class="bg-img">
        <img src="/img/contact.jpg" alt="Buildings">
    </div>
    <span class="bg-overlay"></span>
    <div class="text-wrap">
        <h2>Contact</h2>
    </div>
</section>

<section class="col-two-grid">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div class="wrapper">
                    <div class="card-wrap">
                        <div class="img-wrap">
                            <a href="#">
                                <img src="/img/clock.png" alt="">
                            </a>

                        </div>
                        <div class="content-wrap">
                            <h3>Working Hours</h3>
                            <p>Mon-Fri: 8.00am - 5.30pm</p>
                            <p>Sat: 9.00am - 2.00pm</p>
                        </div>
                    </div>
                    <div class="card-wrap">
                        <div class="img-wrap">
                            <a href="#">
                                <img src="/img/contact.png" alt="">
                            </a>
                        </div>
                        <div class="content-wrap">
                            <h3>Email</h3>
                            <p>muskanmadan143@gmail.com</p>
                            <p>+91-6239-00000</p>
                        </div>
                    </div>
                    <div class="card-wrap wow fadeInUp" data-wow-delay="0.8s">
                        <div class="img-wrap">
                            <a href="#">
                                <img src="/img/map.png" alt="">
                            </a>
                        </div>
                        <div class="content-wrap">
                            <h3>Location</h3>
                            <p>K4 block Rohini, Delhi</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="form-wrap">
                    <input type="text" placeholder="Enter your First name">
                    <input type="text" placeholder="Enter your Last name">
                    <br>
                    <input type="text" placeholder="Enter your Phone">
                    <input type="text" placeholder="Enter your Email">
                    <br>
                    <textarea placeholder="Message">
                    </textarea>
                    <br>
                    <a href="mailto:muskaanmadaan143@gmail.com" class="blue-btn">SEND</a>

                </div>
                <div class="map-wrap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13992.453323846828!2d77.1136773394814!3d28.74603308126403!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d010129b77fb3%3A0x66185923f6ef204b!2sGREEN%20SPARROW!5e0!3m2!1sen!2sin!4v1592160811105!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
        </div>
    </div>

</section>


<?php include('footer.php'); ?>
