<?php include ('header.php'); ?>

<header class="main-nav shadow">
    <div class="container">
        <header class="main-nav shadow">
            <div class="container">
                <nav class="navbar navbar-expand-sm navbar-dark inner-nav">
                    <a class="navbar-brand" href="/">INSURAA</a>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/about.php">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/calculator.php">Calculator</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact.php">Contact</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="/user/index.php" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Login
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/user/index.php">User Login</a>
                                <a class="dropdown-item" href="/user/signup.php">User Sign Up</a>
                                <a class="dropdown-item" href="/admin/index.php">Admin</a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

    </div>
</header>

<section class="hero-banner">
    <div class="bg-img">
        <img src="/img/about.jpg" alt="Buildings">
    </div>
    <span class="bg-overlay"></span>
    <div class="text-wrap">
        <h2>About Us</h2>
    </div>
</section>

<section class="intro">
    <div class="container">
        <div class="row wrapper mr-0">
            <div class="col-12 title-wrap">
                <h2>About company</h2>
            </div>
        </div>
    </div>
</section>

<section class="about-intro">
    <div class="container">
        <div class="row">
            <div class="col-6 content">
                <span>Welcome</span>
                <h4>Welcome to the Best Business Support Company</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="col-6">
                <img src="/img/section-image.jpg" alt="about">
            </div>
        </div>
    </div>
</section>

<section class="intro">
    <div class="container">
        <div class="row wrapper mr-0">
            <div class="col-12 title-wrap">
                <h2>Our Achivements</h2>
            </div>
        </div>
    </div>
</section>

<section class="col-two-content">
    <div class="container">
        <div class="row">
            <div class="col-6 title-wrap">
                <h2>We are Leaders on the Market</h2>
                <p>We are the industry leader in establishing an innovation-friendly organization, developing new business models and new products. The company is on the cutting edge of new technologies.</p>
            </div>
            <div class="col-6 content-wrap">
                <div class="wrapper">
                    <div class="icon-wrap">
                     <img src="/img/icon-chain-clients.svg" alt="icon">
                    </div>
                    <div class="content-wrap">
                        <h4>Satisfied Clients</h4>
                        <p>Excellent customer services</p>
                    </div>
                    <div class="number-wrap">
                        <span>2064</span>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="icon-wrap">
                        <img src="/img/icon-chain-clients.svg" alt="icon">
                    </div>
                    <div class="content-wrap">
                        <h4>Satisfied Clients</h4>
                        <p>Excellent customer services</p>
                    </div>
                    <div class="number-wrap">
                       <span>24</span>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="icon-wrap">
                        <img src="/img/icon-chain-clients.svg" alt="icon">
                    </div>
                    <div class="content-wrap">
                        <h4>Satisfied Clients</h4>
                        <p>Excellent customer services</p>
                    </div>
                    <div class="number-wrap">
                        <span>24</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="intro">
    <div class="container">
        <div class="row wrapper mr-0">
            <div class="col-12 title-wrap">
                    <h2>Team</h2>
            </div>
        </div>
    </div>
</section>

<section class="team">
    <div class="container">
        <div class="row">
            <div class="col-6 card-wrap">
                <div class="img-wrap">
                    <img src="/img/team.jpg" alt="muskaan">
                </div>
                <div class="content-wrap">
                   <h3>Muskaan</h3>
                    <h5 class="designation">Chief Executive Officer & Executive</h5>
                    <p>Orci phasellus egestas tellus rutrum. Accumsan lacus vel facilisis volutpat est.</p>
                    <div class="share">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                    </div>
                </div>
            </div>
            <div class="col-6 card-wrap">
                <div class="img-wrap">
                    <img src="/img/team2.jpg" alt="muskaan">
                </div>
                <div class="content-wrap">
                    <h3>Muskaan</h3>
                    <h5 class="designation">Chief Executive Officer & Executive</h5>
                    <p>Orci phasellus egestas tellus rutrum. Accumsan lacus vel facilisis volutpat est.</p>
                    <div class="share">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                    </div>
                </div>
            </div>
            <div class="col-6 card-wrap">
                <div class="img-wrap">
                    <img src="/img/team4.jpg" alt="muskaan">
                </div>
                <div class="content-wrap">
                    <h3>Muskaan</h3>
                    <h5 class="designation">Chief Executive Officer & Executive</h5>
                    <p>Orci phasellus egestas tellus rutrum. Accumsan lacus vel facilisis volutpat est.</p>
                    <div class="share">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                    </div>
                </div>
            </div>
            <div class="col-6 card-wrap">
                <div class="img-wrap">
                    <img src="/img/team3.jpg" alt="muskaan">
                </div>
                <div class="content-wrap">
                    <h3>Muskaan</h3>
                    <h5 class="designation">Chief Executive Officer & Executive</h5>
                    <p>Orci phasellus egestas tellus rutrum. Accumsan lacus vel facilisis volutpat est.</p>
                    <div class="share">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                        <img src="/img/facebook.png" alt="facebook">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="intro">
    <div class="container">
        <div class="row wrapper mr-0">
            <div class="col-12 title-wrap">
                    <h2>Client Words</h2>
            </div>
        </div>
    </div>
</section>

<section class="client-slider">
    <div class="container">
            <div class="slide-wrap">
                <div class="slide">
                    <div class="card-wrap">
                        <div class="img-wrap">
                            <img src="/img/user.svg" alt="client">
                        </div>
                        <div class="content-wrapper">
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis
                                istpoe.</p>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="card-wrap">
                        <div class="img-wrap">
                            <img src="/img/user.svg" alt="client">
                        </div>
                        <div class="content-wrapper">
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis
                                istpoe.</p>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="card-wrap">
                        <div class="img-wrap">
                            <img src="/img/user.svg" alt="client">
                        </div>
                        <div class="content-wrapper">
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis
                                istpoe.</p>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="card-wrap">
                        <div class="img-wrap">
                            <img src="/img/user.svg" alt="client">
                        </div>
                        <div class="content-wrapper">
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis
                                istpoe.</p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>


<?php include ('footer.php'); ?>

