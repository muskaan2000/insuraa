<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{
if(isset($_POST['submit']))
{
$adminid=$_SESSION['aid'];
$cpassword=$_POST['currentpassword'];
$newpassword=$_POST['newpassword'];
$query=mysqli_query($con,"select ID from tblimsadmin where ID='$adminid' and   Password='$cpassword'");
$row=mysqli_fetch_array($query);
if($row>0){
$ret=mysqli_query($con,"update tblimsadmin set Password='$newpassword' where ID='$adminid'");
$msg= "Your password successully changed"; 
} else {

$msg="Your current password is wrong";
}



}

  
  ?>


<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Change Password</title>

<script type="text/javascript">
function checkpass()
{
if(document.changepassword.newpassword.value!=document.changepassword.confirmpassword.value)
{
alert('New Password and Confirm Password field does not match');
document.changepassword.confirmpassword();
return false;
}
return true;
} 

</script>
  </head>
  <body class="dashboard">
<?php include_once('includes/sidebar.php');?>
<?php include_once('includes/header.php');?>

<section class="pswrd">
    <div class="container">
        <div class="content-header">
            <h2 class="content-title ">Change Password!</h2>

        </div>
        <div class="content-body">
            <div class="row ">
                <div class="col-md-12 col-xl-7">
                    <div class="card card-table-two">
                        <hr />
                        <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                                echo $msg;
                            }  ?> </p>
                        <form method="post" action="" name="changepassword" onsubmit="return checkpass();">
                            <?php
                            $adminid=$_SESSION['aid'];
                            $ret=mysqli_query($con,"select * from tblimsadmin where ID='$adminid'");
                            $cnt=1;
                            while ($row=mysqli_fetch_array($ret)) {

                            ?>

                            <div class="d-flex flex-column wd-md-500 pd-30 pd-sm-40 bg-gray-200">
                                <div class="form-group">
                                    <label class="form-label">Current Password: <span class="tx-danger">*</span></label>
                                    <input type="password" name="currentpassword" class="form-control wd-450" required= "true" value="">
                                </div><br />
                                <div class="form-group">
                                    <label class="form-label">New Password<span class="tx-danger">*</span></label>
                                    <input type="password" name="newpassword" class="form-control wd-450" value="">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Confirm Password<span class="tx-danger">*</span></label>
                                    <input type="password" name="confirmpassword" class="form-control wd-450" value="">
                                </div>
                                <?php } ?>

                                <div class="form-group" align="center">
                                    <button type="submit" name="submit" value="Change" class="btn-default">Change</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>



    <?php include_once('includes/footer.php');?>


    
  </body>
</html>
<?php }  ?>