<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{

if(isset($_POST['update']))
  {
     $sid=$_GET['scid'];
    $catid=$_POST['category'];
    $subcat=$_POST['subcategory'];

     
    $query=mysqli_query($con, "update tblsubcategory set CategoryId='$catid', SubcategoryName='$subcat' where id='$sid'");
    if ($query) {
    $msg="Sub Category has been updated.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again";
    }

  
}
  ?>

<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Sub Category</title>



  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="category">
    <div class="container">
        <div class="content-header ">
            <h2 class="content-title ">Sub Category !</h2>
        </div>
        <div class="content-body">
            <p style="font-size:16px; color:red" align="left"> <?php if($msg){
                    echo $msg;
                }  ?> </p>

            <div class="row ">
                <div class="col-md-12 ">
                    <div class="card ">
                        <form method="post">
                            <?php
                            $sid=$_GET['scid'];
                            $ret=mysqli_query($con,"select category.CategoryName as catname,category.ID as cid,tblsubcategory.SubcategoryName as subcat from tblsubcategory inner join category on category.ID=tblsubcategory.CategoryId where tblsubcategory.id='$sid'");

                            while ($row=mysqli_fetch_array($ret)) {

                            ?>
                            <div class="d-flex flex-column wd-md-500 pd-30 pd-sm-40 bg-gray-200">
                                <div class="form-group">
                                    <label class="form-label">Sub Category <span class="tx-danger">*</span></label>
                                    <select name="category" class="form-control wd-450" required="true">
                                        <option value="<?php echo $row['cid'];?>"><?php echo $row['catname'];?></option>
                                        <?php $query=mysqli_query($con,"select * from category");
                                        while($result=mysqli_fetch_array($query))
                                        {
                                            ?>
                                            <option value="<?php echo $result['ID'];?>"><?php echo $result['CategoryName'];?></option>
                                        <?php } ?>
                                    </select>
                                </div><br />


                                <div class="form-group">
                                    <label class="form-label">Sub Category Name: <span class="tx-danger">*</span></label>
                                    <input type="text" name="subcategory" class="form-control wd-450" required="true" value="<?php echo $row['subcat'];?>">
                                </div>

                                <?php }?>
                                <div class="form-group" align="center">
                                    <button type="submit" name="update" class="btn-default">Update</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>



    <?php include_once('includes/footer.php');?>



    
  </body>
</html>
<?php  } ?>
