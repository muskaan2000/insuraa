<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{


?>






<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Insuraa |   DisApproved Policy </title>


  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>
<section class="policy">
    <div class="container">
        <div class="content-header">

            <h2 class="content-title "> Disapproved Policy!</h2>

        </div>
        <div class="content-body">



            <div class="content-label mg-b-5"> Disapproved Policy</div>
            <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                    echo $msg;
                }  ?> </p>
            <div class="table-responsive">
                <table class="table table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Policy Holder Name</th>
                        <th>Policy Holder Contact No.</th>
                        <th>Policy Holder Gender</th>
                        <th>Policy Name</th>
                        <th>Policy Number</th>
                        <th>Category Name</th>
                        <th>SubCategory Name</th>

                        <th>Sum Assured</th>
                        <th>Premium</th>
                        <th>Tenure</th>
                        <th>Apply Date</th>
                        <th>Status</th>

                    </tr>
                    </thead>
                    <?php
                    $uid=$_SESSION['uid'];
                    $policynumber = mt_rand(100000000, 999999999);
                    $ret=mysqli_query($con,"select tbluser.FullName,tbluser.ContactNo,tbluser.Gender, category.CategoryName as catname,tblsubcategory.SubcategoryName as subcat, tblpolicy.PolicyName,tblpolicyholder.PolicyApplyDate as applydate,tblpolicy.ID,tblpolicy.Sumassured,tblpolicy.Premium,tblpolicy.Tenure,tblpolicyholder.PolicyStatus,tblpolicyholder.PolicyNumber,tblpolicyholder.ID as tphid  from tblpolicy inner join category on category.ID=tblpolicy.CategoryId inner join tblsubcategory on  tblsubcategory.id=tblpolicy.SubcategoryId  join tblpolicyholder on tblpolicyholder.PolicyId=tblpolicy.ID join tbluser on tbluser.ID=tblpolicyholder.UserId where  tblpolicyholder.PolicyStatus='2'");
                    $cnt=1;
                    while ($row=mysqli_fetch_array($ret)) {

                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo $cnt;?></td>
                        <td><?php  echo $row['FullName'];?></td>
                        <td><?php  echo $row['ContactNo'];?></td>
                        <td><?php  echo $row['Gender'];?></td>

                        <td><?php  echo $row['PolicyName'];?></td>
                        <td><?php  echo $row['PolicyNumber'];?></td>
                        <td><?php  echo $row['catname'];?></td>
                        <td><?php  echo $row['subcat'];?></td>

                        <td><?php  echo $row['Sumassured'];?></td>
                        <td><?php  echo $row['Premium'];?></td>
                        <td><?php  echo $row['Tenure'];?></td>
                        <td><?php  echo $row['applydate'];?></td>
                        <td><?php
                            if($row['PolicyStatus']=="0"){
                                echo "waiting for approval";
                            }

                            if($row['PolicyStatus']=="1"){
                                echo "approved";
                            }

                            if($row['PolicyStatus']=="2"){
                                echo "Disapproved";
                            }
                            ?></td>

                    </tr>
                    <?php
                    $cnt=$cnt+1;
                    }?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>



    <?php include_once('includes/footer.php');?>


    
  </body>
</html>
<?php }  ?>