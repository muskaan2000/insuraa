<?php include_once('../header.php'); ?>

<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{

if(isset($_POST['submit']))
  {
    $category=$_POST['categoryname'];
     
    $query=mysqli_query($con, "insert into category(CategoryName) value('$category')");
    if ($query) {
    $msg="Category has been created.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again";
    }

  
}
  ?>


<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Insuraa |  Add Category</title>



  </head>
  <body class="dashboard">
<?php include_once('includes/sidebar.php');?>
<?php include_once('includes/header.php');?>

<section class="category">
    <div class="container">
    <div class="content-header ">
        <h2 class="content-title ">Add Insurance Category !</h2>
    </div>
    <div class="content-body">
        <div class="row ">
            <div class="col-md-12 col-xl-7">
                <div class="card">
                    <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                            echo $msg;
                        }  ?> </p>
                    <form method="post" action="">
                                <div class="form-group">
                                    <label class="form-label">Category Name: <span class="tx-danger">*</span></label>
                                    <input type="text" name="categoryname" class="form-control wd-550" placeholder="Enter Category Name" required= "true">
                                </div>

                            <button type="submit" name="submit" class="btn-default">Submit</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
    </div>
</section>



    <?php include_once('includes/footer.php');?>




    
  </body>
</html>
<?php }  ?>