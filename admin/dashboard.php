<?php include_once('../header.php'); ?>
<?php  
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{
    ?>
<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Dashboard</title>

  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>
    <div class="content content-dashboard-two">
    <?php include_once('includes/header.php');?>

        <section class="admin-dashboard">
            <div class="container">
                <div class="content-body">

                    <div class="row row-sm">
                        <div class="col-sm-4">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-purple"><i class="typcn typcn-user"></i></div>
                                    <div class="media-body">
                                        <?php $query=mysqli_query($con,"Select * from tbluser");
                                        $usercount=mysqli_num_rows($query);
                                        ?>

                                        <h6><?php echo $usercount;?> </h6>
                                        <span>Total User</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart1" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-primary"><i class="typcn typcn-chart-line-outline"></i></div>
                                    <div class="media-body">
                                        <?php $ret=mysqli_query($con,"Select * from tblpolicy");
                                        $policycount=mysqli_num_rows($ret);
                                        ?>

                                        <h6><?php echo $policycount;?></h6>
                                        <span>Listed Polcies</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart2" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-pink"><i class="typcn typcn-chart-line-outline"></i></div>
                                    <div class="media-body">
                                        <?php $rt=mysqli_query($con,"Select * from category");
                                        $catcount=mysqli_num_rows($rt);
                                        ?>
                                        <h6><?php echo $catcount;?> </h6>
                                        <span>Listed Categories</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart3" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-teal"><i class="typcn typcn-chart-line-outline"></i></div>
                                    <div class="media-body">
                                        <?php $rw=mysqli_query($con,"Select * from tblsubcategory");
                                        $subcatcount=mysqli_num_rows($rw);
                                        ?>

                                        <h6><?php echo $subcatcount;?></h6>
                                        <span>Listed Sub Categories</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart4" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-purple"><i class="typcn typcn-user"></i></div>
                                    <div class="media-body">
                                        <?php $qy=mysqli_query($con,"Select * from tblpolicyholder");
                                        $tregpolicy=mysqli_num_rows($qy);
                                        ?>

                                        <h6><?php echo $tregpolicy;?> </h6>
                                        <span>Policy Holder</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart1" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-primary"><i class="typcn typcn-chart-line-outline"></i></div>
                                    <div class="media-body">
                                        <?php $ret1=mysqli_query($con,"Select * from tblpolicyholder where PolicyStatus=1");
                                        $atregpolicy=mysqli_num_rows($ret1);
                                        ?>

                                        <h6><?php echo $atregpolicy;?></h6>
                                        <span>Total Approved Policy Holder</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart2" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-pink"><i class="typcn typcn-chart-line-outline"></i></div>
                                    <div class="media-body">
                                        <?php $ret2=mysqli_query($con,"Select * from tblpolicyholder where PolicyStatus=2");
                                        $adtregpolicy=mysqli_num_rows($ret2);
                                        ?>

                                        <h6><?php echo $adtregpolicy;?> </h6>
                                        <span>DisApproved Policy Holder</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart3" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card admin">
                                <div class="media">
                                    <div class="media-icon bg-teal"><i class="typcn typcn-chart-line-outline"></i></div>
                                    <div class="media-body">
                                        <?php $ret0=mysqli_query($con,"Select * from tblpolicyholder where PolicyStatus=0");
                                        $pendingpolicy=mysqli_num_rows($ret0);
                                        ?>

                                        <h6><?php echo $pendingpolicy;?></h6>
                                        <span>Waiting for approval</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart4" class="flot-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

<?php include_once('includes/footer.php');?>
    </div>

    <script src="../js/dashboard.sampledata.js"></script>
    <script>
      $(function(){
        'use strict'

        $('.sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('sidebar-show');
            }
          }
        });


        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('body').toggleClass('sidebar-hide');
          } else {
            $('body').toggleClass('sidebar-show');
          }
        });

        new PerfectScrollbar('.sidebar-body', {
          suppressScrollX: true
        });

        /* ----------------------------------- */
        /* Dashboard content */


        $.plot('#flotChart1', [{
            data: dashData1,
            color: '#6f42c1'
          }], {
    			series: {
    				shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 1 } ] }
            }
    			},
          grid: {
            borderWidth: 0,
            labelMargin: 0
          },
    			yaxis: {
            show: false,
            min: 0,
            max: 100
          },
    			xaxis: { show: false }
    		});

        $.plot('#flotChart2', [{
            data: dashData2,
            color: '#007bff'
          }], {
    			series: {
    				shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 1 } ] }
            }
    			},
          grid: {
            borderWidth: 0,
            labelMargin: 0
          },
    			yaxis: {
            show: false,
            min: 0,
            max: 100
          },
    			xaxis: { show: false }
    		});

        $.plot('#flotChart3', [{
            data: dashData3,
            color: '#f10075'
          }], {
    			series: {
    				shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 1 } ] }
            }
    			},
          grid: {
            borderWidth: 0,
            labelMargin: 0
          },
    			yaxis: {
            show: false,
            min: 0,
            max: 100
          },
    			xaxis: { show: false }
    		});

        $.plot('#flotChart4', [{
            data: dashData4,
            color: '#00cccc'
          }], {
    			series: {
    				shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 1 } ] }
            }
    			},
          grid: {
            borderWidth: 0,
            labelMargin: 0
          },
    			yaxis: {
            show: false,
            min: 0,
            max: 100
          },
    			xaxis: { show: false }
    		});

        $.plot('#flotChart5', [{
            data: dashData2,
            color: '#00cccc'
          },{
            data: dashData3,
            color: '#007bff'
          },{
            data: dashData4,
            color: '#f10075'
          }], {
    			series: {
    				shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: false,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 1 } ] }
            }
    			},
          grid: {
            borderWidth: 0,
            labelMargin: 20
          },
    			yaxis: {
            show: false,
            min: 0,
            max: 100
          },
    			xaxis: {
            show: true,
            color: 'rgba(0,0,0,.16)',
            ticks: [
              [0, ''],
              [10, '<span>Nov</span><span>05</span>'],
              [20, '<span>Nov</span><span>10</span>'],
              [30, '<span>Nov</span><span>15</span>'],
              [40, '<span>Nov</span><span>18</span>'],
              [50, '<span>Nov</span><span>22</span>'],
              [60, '<span>Nov</span><span>26</span>'],
              [70, '<span>Nov</span><span>30</span>'],
            ]
          }
    		});

        $.plot('#flotChart6', [{
            data: dashData2,
            color: '#6f42c1'
          },{
            data: dashData3,
            color: '#007bff'
          },{
            data: dashData4,
            color: '#00cccc'
          }], {
    			series: {
    				shadowSize: 0,
            stack: true,
            bars: {
              show: true,
              lineWidth: 0,
              fill: 0.85
              //fillColor: { colors: [ { opacity: 0 }, { opacity: 1 } ] }
            }
    			},
          grid: {
            borderWidth: 0,
            labelMargin: 20
          },
    			yaxis: {
            show: false,
            min: 0,
            max: 100
          },
    			xaxis: {
            show: true,
            color: 'rgba(0,0,0,.16)',
            ticks: [
              [0, ''],
              [10, '<span>Nov</span><span>05</span>'],
              [20, '<span>Nov</span><span>10</span>'],
              [30, '<span>Nov</span><span>15</span>'],
              [40, '<span>Nov</span><span>18</span>'],
              [50, '<span>Nov</span><span>22</span>'],
              [60, '<span>Nov</span><span>26</span>'],
              [70, '<span>Nov</span><span>30</span>'],
            ]
          }
    		});

        $('#vmap').vectorMap({
          map: 'world_en',
          showTooltip: true,
          backgroundColor: '#f8f9fa',
          color: '#ced4da',
          colors: {
            us: '#6610f2',
            gb: '#8b4bf3',
            ru: '#aa7df3',
            cn: '#c8aef4',
            au: '#dfd3f2'
          },
          hoverColor: '#222',
          enableZoom: false,
          borderOpacity: .3,
          borderWidth: 3,
          borderColor: '#fff',
          hoverOpacity: .85
        });

      });
    </script>
  </body>
</html>
<?php } ?>