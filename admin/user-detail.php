<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{

?>





<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Insuraa |  Mange Use Detail</title>



  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="user">
    <div class="container">
        <div class="content-header ">

            <h2 class="content-title ">Manage User Detail !</h2>


        </div>
        <div class="content-body">

            <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                    echo $msg;
                }  ?> </p>
            <div class="table-responsive">
                <table class="table table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Full Name</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Creation Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <?php
                    $ret=mysqli_query($con,"select * from  tbluser");
                    $cnt=1;
                    while ($row=mysqli_fetch_array($ret)) {

                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo $cnt;?></td>

                        <td><?php  echo $row['FullName'];?></td>
                        <td><?php  echo $row['ContactNo'];?></td>
                        <td><?php  echo $row['Email'];?></td>
                        <td><?php  echo $row['Gender'];?></td>
                        <td><?php  echo $row['CreationDate'];?></td>
                        <td><a href="edit-userprofile.php?userid=<?php echo $row['ID'];?>">Edit User Detail</a>
                    </tr>
                    <?php
                    $cnt=$cnt+1;
                    }?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>


    <?php include_once('includes/footer.php');?>




    
  </body>
</html>
<?php }  ?>