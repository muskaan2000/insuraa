<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{

if(isset($_POST['submit']))
  {
    $catid=$_POST['category'];
    $subcat=$_POST['subcategory'];
    
    $polname=$_POST['policyname'];
    $sumass=$_POST['sumassured'];
    $pri=$_POST['premium'];
    $tenure=$_POST['tenure'];
     
    $query=mysqli_query($con, "insert into tblpolicy(CategoryId, SubcategoryId, PolicyName, Sumassured, Premium,Tenure) value('$catid','$subcat','$polname','$sumass','$pri','$tenure')");
    if ($query) {
    $msg="Policy form has been summitted.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again";
    }

  
}
  ?>

<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Policy Form!!</title>


 <script>
function getSubCat(val) {
  $.ajax({
type:"POST",
url:"get-subcat.php",
data:'catid='+val,
success:function(data){
$("#subcategory").html(data);
}

  });


}
  
  
  </script>

  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="policy-form">
    <div class="container">
        <div class="content-header ">
            <h2 class="content-title ">Policy Form !</h2>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form method="post">
                            <p style="font-size:16px; color:red" align="left"> <?php if($msg){
                                    echo $msg;
                                }  ?> </p>
                            <div class="d-flex flex-column wd-md-500 pd-30 pd-sm-40 bg-gray-200">
                                <div class="form-group">
                                    <label class="form-label">Category <span class="tx-danger">*</span></label>
                                    <select name="category" id="category" onChange="getSubCat(this.value)" class="form-control wd-450" required="true">
                                        <option value="">Select Category</option>
                                        <?php $query=mysqli_query($con,"select * from category");
                                        while($row=mysqli_fetch_array($query))
                                        {
                                            ?>
                                            <option value="<?php echo $row['ID'];?>"><?php echo $row['CategoryName'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label class="form-label">Sub Category Name: <span class="tx-danger">*</span></label>
                                    <select name="subcategory" id="subcategory" class="form-control wd-450" required="true">
                                        <option value="">Select Sub Category</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Policy Name: <span class="tx-danger">*</span></label>
                                    <input type="text" name="policyname" class="form-control wd-450" required="true">
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Sum Assured: <span class="tx-danger">*</span></label>
                                    <input type="text" name="sumassured" class="form-control wd-450" required="true">
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Premium: <span class="tx-danger">*</span></label>
                                    <input type="text" name="premium" class="form-control wd-450" required="true">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Tenure: <span class="tx-danger">*</span></label>
                                    <input type="text" name="tenure" class="form-control wd-450" required="true">
                                </div>




                                <div class="form-group" align="center">
                                    <button type="submit" name="submit" class="btn-default">Submit</button>
                                </div>
                            </div></div>
                </div><!-- d-flex -->

                </form>

            </div>
        </div>
    </div>
</section>

    <?php include_once('includes/footer.php');?>


    
  </body>
</html>
<?php  } ?>
