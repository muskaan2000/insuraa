<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{


if(isset($_POST['submit']))
  {
    $adminid=$_SESSION['aid'];
    $AName=$_POST['adminname'];
  
  
     $query=mysqli_query($con, "update tblimsadmin set AdminName ='$AName' where ID='$adminid'");
    if ($query) {
    $msg="Admin profile has been updated.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again.";
    }
  }
  ?>


<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Admin Profile</title>


  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>
<?php include_once('includes/header.php');?>

<section class="profile">
    <div class="content-header">
            <h2 class="content-title">Admin Profile !</h2>

    </div>
    <div class="content-body">
        <div class="row ">
            <div class="col-md-12 col-xl-7">
                <div class="card card-table-two">
                    <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                            echo $msg;
                        }  ?> </p>
                    <form method="post" action="" name="changepassword">
                        <?php
                        $adminid=$_SESSION['aid'];
                        $ret=mysqli_query($con,"select * from tblimsadmin where ID='$adminid'");
                        $cnt=1;
                        while ($row=mysqli_fetch_array($ret)) {

                        ?>

                        <div class="d-flex flex-column wd-md-500 pd-30 pd-sm-40 bg-gray-200">
                            <div class="form-group">
                                <label class="form-label">Admin Name: <span class="tx-danger">*</span></label>
                                <input type="text" name="adminname" class="form-control wd-450" required= "true" value="<?php  echo $row['AdminName'];?>">
                            </div><br />
                            <div class="form-group">
                                <label class="form-label">User Name: <span class="tx-danger">*</span></label>
                                <input type="text" name="username" class="form-control wd-450" readonly="true" value="<?php  echo $row['AdminUsername'];?>">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Admin Regestration date <span class="tx-danger">*</span></label>
                                <input type="text" name="AdminRegdate" class="form-control wd-450" readonly= "true" value="<?php  echo $row['AdminRegdate'];?>">
                            </div>
                            <?php } ?>

                            <div class="form-group" align="center">
                                <button type="submit" name="submit" class="btn-default">Update</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

        </div>
    </div>
</section>


    <?php include_once('includes/footer.php');?>



    
  </body>
</html>
<?php }  ?>