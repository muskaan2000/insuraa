<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{

?>





<!DOCTYPE html>
<html lang="en">
  <head>


    <title>Insuraa |  Manage Category</title>



  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="user">
    <div class="container">
        <div class="content-header ">

            <h2 class="content-title ">Manage SubCategories !</h2>


        </div>
        <div class="content-body">

            <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                    echo $msg;
                }  ?> </p>
            <div class="table-responsive">
                <table class="table table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Category Name</th>
                        <th>SubCategory Name</th>
                        <th>Creation Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <?php
                    $ret=mysqli_query($con,"select category.CategoryName as catname,tblsubcategory.SubcategoryName as subcat,tblsubcategory.CreationDate as cdate,tblsubcategory.id as subcatid from tblsubcategory inner join category on category.ID=tblsubcategory.CategoryId");
                    $cnt=1;
                    while ($row=mysqli_fetch_array($ret)) {

                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo $cnt;?></td>

                        <td><?php  echo $row['catname'];?></td>
                        <td><?php  echo $row['subcat'];?></td>
                        <td><?php  echo $row['cdate'];?></td>
                        <td><a href="edit-subcategory.php?scid=<?php echo $row['subcatid'];?>">Edit Insurance Category</a>
                    </tr>
                    <?php
                    $cnt=$cnt+1;
                    }?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>

    <?php include_once('includes/footer.php');?>



    
  </body>
</html>
<?php }  ?>