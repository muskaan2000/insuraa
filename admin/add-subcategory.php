<?php include_once('../header.php'); ?>
<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{

if(isset($_POST['submit']))
  {
    $catid=$_POST['category'];
    $subcat=$_POST['subcategory'];

     
    $query=mysqli_query($con, "insert into tblsubcategory(CategoryId, SubcategoryName) value('$catid','$subcat')");
    if ($query) {
    $msg="Sub Category has been created.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again";
    }

  
}
  ?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Sub Category</title>


  </head>
  <body class="dashboard">
<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="category">
    <div class="container">

        <div class="content-header">
                <h2 class="content-title">Sub Category !</h2>

    </div>
        <div class="content-body">
            <p style="font-size:16px; color:red" align="left"> <?php if($msg){
                    echo $msg;
                }  ?> </p>

            <div class="row ">
                <div class="col-md-12 col-xl-7">
                    <div class="card">
                        <form method="post">

                            <div class="d-flex flex-column wd-md-500 pd-30 pd-sm-40 bg-gray-200">
                                <div class="form-group">
                                    <label class="form-label">Category <span class="tx-danger">*</span></label>
                                    <select name="category" class="form-control wd-450" required="true" >
                                        <option value="">Select Category</option>
                                        <?php $query=mysqli_query($con,"select * from category");
                                        while($row=mysqli_fetch_array($query))
                                        {
                                            ?>
                                            <option value="<?php echo $row['ID'];?>"><?php echo $row['CategoryName'];?></option>
                                        <?php } ?>
                                    </select>
                                </div><br />

                                <div class="form-group">
                                    <label class="form-label">Sub Category Name: <span class="tx-danger">*</span></label>
                                    <input type="text" name="subcategory" class="form-control wd-450" required="true">
                                </div>


                                <div class="form-group" align="center">
                                    <button type="submit" name="submit" class="btn-default">Add</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
</section>


    <?php include_once('includes/footer.php');?>



    
  </body>
</html>
<?php  } ?>
