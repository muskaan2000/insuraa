<div class="sidebar">
    <div class="sidebar-header">
        <div class="toggle-menu">
            <img src="/img/toggle.svg" alt="toggle">
        </div>
    </div>
    <div class="sidebar-loggedin">
        <div class="img-user online"><img src="/img/user.svg" alt=""></div>
        <div class="media-body">

            <?php
            $adminid = $_SESSION['aid'];
            $ret = mysqli_query($con, "select AdminName from tblimsadmin where ID='$adminid'");
            $row = mysqli_fetch_array($ret);
            $name = $row['AdminName'];

            ?>
            <span><?php echo $name; ?></span>
            <span>| Admin</span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="sidenav">
            <li>
                <a href="/admin/dashboard.php" class="head dash"><i
                            class="typcn typcn-chart-bar-outline"></i>Dashboard</a>
            </li>
            <li>
                <a href="#" class="head"><i class="typcn typcn-clipboard"></i>Insurance Category</a>
            </li>

            <li>
                <a href="/admin/add-category.php" class="nav-sub-link">Add Category</a>
            </li>
            <li>
                <a href="/admin/manage-categories.php" class="nav-sub-link">Manage Categories</a>

            </li>
            <li>
                <a href="#" class="head"><i class="typcn typcn-clipboard"></i>Insurance SubCategory</a>
            </li>

            <li>
                <a href="/admin/add-subcategory.php" class="nav-sub-link">Add SubCategory</a>
            </li>
            <li>
                <a href="/admin/manage-subcategories.php" class="nav-sub-link">Manage SubCategories</a>

            </li>
            <li><a href="#" class="head"><i class="typcn typcn-clipboard"></i>Insurance Policy</a>
            <li>
                <a href="/admin/policyform.php" class="nav-sub-link">Add Policy</a>
            </li>
            <li>
                <a href="/admin/manage-policyform.php" class="nav-sub-link">Manage Policy</a>

            </li>

            <li>
                <a href="" class="head"><i class="typcn typcn-clipboard"></i>User Details</a>
            </li>
            <li>
                <a href="/admin/user-detail.php">All Users</a>

            </li>


            <li>
                <a href="#" class="head"><i class="typcn typcn-clipboard"></i>Policy Holders</a>
            <li>
                <a href="/admin/pending-policy.php" class="nav-sub-link">Pending Policy</a>
            </li>
            <li>
                <a href="/admin/approve-policy.php" class="nav-sub-link">Approved Policy</a>
            </li>
            <li>
                <a href="/admin/disapprove-policy.php" class="nav-sub-link">Disapproved Policy</a>

            </li>


            <li>
                <a href="" class="head"><i class="typcn typcn-clipboard"></i>Ticket</a>
            </li>

            <li class="nav-sub">
                <a href="/admin/unresolved-tickets.php">UnResolved Tickets</a>
            </li>
            <li>
                <a href="/admin/resolved-tickets.php">Resolved Tickets</a>
            </li>


        </ul>
    </div>
</div>