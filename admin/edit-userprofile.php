<?php include_once('../header.php');?>

<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['aid']==0)) {
  header('location:logout.php');
  } else{

if(isset($_POST['submit']))
  {
    
     $eid=$_GET['userid'];
     $FName=$_POST['fullname'];
    $ContactNo=$_POST['contactnumber'];
    $email=$_POST['email'];
    $gender=$_POST['gender'];
     
   $query=mysqli_query($con, "update tbluser set FullName='$FName',  ContactNo='$ContactNo',  Email='$email', Gender='$gender' where ID='$eid'");
    if ($query) {
    $msg="User detail has been updated.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again";
    }

  
}
  ?>


<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Insuraa |  Update User Detail</title>


  </head>
  <body class="dashboard">

<?php include_once('includes/sidebar.php');?>

<?php include_once('includes/header.php');?>

<section class="user">
    <div class="container">
        <div class="content-header ">
            <h2 class="content-title ">Edit User Detail !</h2>
        </div>

        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                                echo $msg;
                            }  ?> </p>
                        <form method="post" action="">
                            <?php
                            $cid=$_GET['userid'];
                            $ret=mysqli_query($con,"select * from tbluser where ID='$cid'");
                            $cnt=1;
                            while ($row=mysqli_fetch_array($ret)) {

                            ?>
                            <div class="wd-sm-600">
                                <div class="d-md-flex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Full Name <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter your Full Name"  name="fullname" required="true" value="<?php  echo $row['FullName'];?>">
                                    </div></div>

                                <div class="d-md-flex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Contact Number <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter your Contact Number"  name="contactnumber" maxlength="10" pattern="[0-9]+" required="true" value="<?php  echo $row['ContactNo'];?>">
                                    </div></div>
                                <div class="d-md-flex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Email <span class="tx-danger">*</span></label>
                                        <input type="email" class="form-control" placeholder="Enter your Email"  name="email"  required="true" value="<?php  echo $row['Email'];?>">
                                    </div></div>

                                <div class="d-md-flex mg-b-20">
                                    <div class="form-group mg-b-0">
                                        <label class="form-label">Gender <span class="tx-danger">*</span></label>
                                        <?php if($row['Gender']=="Male")
                                        {?>
                                            <input type="radio" id="gender" name="gender" value="Male" checked="true">Male

                                            <input type="radio" name="gender" value="Female">Female
                                        <?php }   if($row['Gender']=="Female") {?>
                                            <input type="radio" id="gender" name="gender" value="Male" >Male
                                            <input type="radio" name="gender" value="Female" checked="true">Female
                                        <?php }?>
                                    </div></div>

                                <?php } ?>
                                <button type="submit" name="submit" class="btn-default">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>



    <?php include_once('includes/footer.php');?>




    
  </body>
</html>
<?php }  ?>