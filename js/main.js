$(document).ready(function () {


    // ------------------menu-------------------------
    $(function(){
        'use strict'

        // This template is mobile first so active menu in navbar
        // has submenu displayed by default but not in desktop
        // so the code below will hide the active menu if it's in desktop
        if(window.matchMedia('(min-width: 992px)').matches) {
            $('.navbar .active').removeClass('show');
        }

        // Shows header dropdown while hiding others
        $('.header .dropdown > a').on('click', function(e) {
            e.preventDefault();
            $(this).parent().toggleClass('show');
            $(this).parent().siblings().removeClass('show');
        });

        // Showing submenu in navbar while hiding previous open submenu
        $('.navbar .with-sub').on('click', function(e) {
            e.preventDefault();
            $(this).parent().toggleClass('show');
            $(this).parent().siblings().removeClass('show');
        });

        // this will hide dropdown menu from open in mobile
        $('.dropdown-menu .header-arrow').on('click', function(e){
            e.preventDefault();
            $(this).closest('.dropdown').removeClass('show');
        });

        // this will show navbar in left for mobile only
        $('#NavShow, #NavbarShow').on('click', function(e){
            e.preventDefault();
            $('body').addClass('navbar-show');
        });

        // this will hide currently open content of page
        // only works for mobile
        $('#ContentLeftShow').on('click touch', function(e){
            e.preventDefault();
            $('body').addClass('content-left-show');
        });

        // This will hide left content from showing up in mobile only
        $('#ContentLeftHide').on('click touch', function(e){
            e.preventDefault();
            $('body').removeClass('content-left-show');
        });

        // this will hide content body from showing up in mobile only
        $('#ContentBodyHide').on('click touch', function(e){
            e.preventDefault();
            $('body').removeClass('content-body-show');
        })

        // navbar backdrop for mobile only
        $('body').append('<div class="navbar-backdrop"></div>');
        $('.navbar-backdrop').on('click touchstart', function(){
            $('body').removeClass('navbar-show');
        });

        // Close dropdown menu of header menu
        $(document).on('click touchstart', function(e){
            e.stopPropagation();

            // closing of dropdown menu in header when clicking outside of it
            var dropTarg = $(e.target).closest('.header .dropdown').length;
            if(!dropTarg) {
                $('.header .dropdown').removeClass('show');
            }

            // closing nav sub menu of header when clicking outside of it
            if(window.matchMedia('(min-width: 992px)').matches) {
                var navTarg = $(e.target).closest('.navbar .nav-item').length;
                if(!navTarg) {
                    $('.navbar .nav-item').removeClass('show');
                }
            }
        });


    });


    //banner
    $('.banner .slide-wrap').slick({
        arrows: true,
        dots: true,
        slidesToShow: 1,
        autoPlay: true,
        autoplaySpeed: 2500,
        infinte: true
    })


    // content with image
    let iconBlock = $(".content-with-icon .row-wrap .text-with-icon");
    let mainImgWrap = $(".content-with-icon .row-wrap .main-img-wrap");

    iconBlock.hover(function () {
        let index = $(this).index();
        if ($(this).parent().hasClass("right-col")) {
            index += 3
        }

        iconBlock.removeClass('active');
        $(this).addClass('active');
        mainImgWrap.find("img").hide();
        mainImgWrap.find("img").eq(index).show();
    });

    //tabs
    $('.tabs ul li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');

    })

    //form
    var signUp = $("#signUp");
    var signIn = $("#signIn");
    var container = $(".form-wrap");

    signUp.click(function () {
        container.addClass("right-panel-active");
        console.log("signun")
    });
    signIn.click(function () {
        container.removeClass("right-panel-active");
        console.log("signin")
    });


    //card slider

    var cards = $(".card-slider .slide-wrap .slide");
    if (cards.length > 3) {
        $(".card-slider .slide-wrap").slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            arrows: true,
            draggable: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                }
            ]

        });
    }
    //card slider
    $(window).on('load', function () {
        $('.client-slider .slide-wrap').slick({
            arrows: true,
            slidesToShow: 1,
            infinite: true,
            centerMode: true,
            responsive: [
                {
                    breakpoint: 576,
                    settings: {
                        dots: true,
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });

    // sidebar toggle
    var toggle = $(".sidebar .sidebar-header .toggle-menu");
    var sidebar = $(".sidebar");
    toggle.click(function(){
        sidebar.toggleClass("collapsed");
    });

    var totalAmount, rateOfInterest, time;

    $('#submit').on('click', function () {
       totalAmount = parseInt($('#total-amount').val(), 10);
       rateOfInterest = parseInt($('#rate-of-interest').val(), 10);
       time = parseInt($('#time').val(), 10);

       var emi = totalAmount * (rateOfInterest/100) * time;

       alert("Total EMI is: " + emi);
    });
});


