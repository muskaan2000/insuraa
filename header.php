
<head>
    <meta name="description" content="Insurance Management System in PHP and MySQL">
    <meta name="author" content="Muskaan Madaan">

    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/style.css">
<!--    <link rel="icon" href=/"assets/images/favicon.svg" type="image/gif">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/slick.min.js"></script>
</head>


<script type="text/javascript" src="/js/main.js"></script>