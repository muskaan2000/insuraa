**You can clone from Bitbucket **
https://bitbucket.org/muskaan2000/insuraa/src/master/



---

## Database

You’ll get dtabase attached in the sql folder.


---

## Upload database (For XAMP/MAMP pro)

1. Download the database.
2. Go to localhost/phphmyadmin. 
3. Select the import tab and click import button. 
4. Choose the .sql file and import. 
5. Your database is now set and ready to use.


---

## Setting up project (For XAMP)

1. Create the project.
2. Put in inside htdocs folder in XAMP.
3. Go to localhost/insuraa

---

## Setting up project (FOR MAMP pro)

1. Clone the project
2. Create a localhost named insuraa.test
3. Upload the database.
4. Run insuraa.test.

---

## Instructions

1. ## Admin Credentials

Username: Admin
Password: mus143


2. ## Demo User Credentials

Username: testuser@gmail.com
Password: Test@123